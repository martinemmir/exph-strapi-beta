module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 5337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', 'f22bb6440f007d16078001c12d4039cc'),
    },
  },
});
