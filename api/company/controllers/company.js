const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

module.exports = {
  /**
   * Create a record.
   *
   * @return {Object}
   */

  async create(ctx) {
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
    //   data.author = ctx.state.user.id;
      entity = await strapi.services.company.create(data, { files });
    } else {
    //   ctx.request.body.author = ctx.state.user.id;
      entity = await strapi.services.company.create(ctx.request.body);
    }
    return sanitizeEntity(entity, { model: strapi.models.company });
  },
};
